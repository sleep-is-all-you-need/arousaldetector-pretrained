# Pretrained models

Pretrained models for the [arousaldetector](https://gitlab.com/sleep-is-all-you-need/arousaldetector) project.

Contains:
- `modelState*` - Four different models trained on DSDS, SHHS, MESA and MrOS. Files are torch compatible and can be loaded using the `torch.load` function.
- `threshHold*` - The different threshold (optimized for F1 on the validation sets). Files are pickle compatible and can be loaded using the `pickle.load` function and contain a float value.


## Setup

These files are expected to be in the `data` directory where you execute the `arousaldetector` project:

`git clone https://gitlab.com/sleep-is-all-you-need/arousaldetector-pretrained.git data`